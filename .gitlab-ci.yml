stages:
  - dist
  - source
  - build
  - lint

# include job templates from the gitlab-ci-templates repo
# see https://computing.docs.ligo.org/gitlab-ci-templates/
include:
  - project: computing/gitlab-ci-templates
    file:
      # helpers for debian builds
      - /packaging/debian.yml
      # helpers for RHEL builds
      - /packaging/rhel.yml
      # a flake8 job template
      - /python/lint.yml

cache:
  key: "${CI_JOB_NAME}"
  paths:
    - .cache

# -- dist -------------------

tarball:
  stage: dist
  image: igwn/lalsuite-dev:el7
  needs: []
  extends:
    - .rhel:base
  variables:
    GIT_STRATEGY: fetch
  before_script:
    # configure yum cache etc
    - !reference [".rhel:base", before_script]
    # install build requirements
    - yum -y -q install
          liblal-dev
  script:
    - ./00boot
    - ./configure
    - make dist
  artifacts:
    paths:
      - lalapps*.tar.*

# -- source -----------------

.source:
  stage: source
  needs:
    - tarball

.srpm:
  extends:
    - .rhel:srpm
    - .source
  variables:
    EPEL: "true"

srpm:el7:
  extends:
    - .srpm
  image: centos:7

.dsc:
  extends:
    - .debian:dsc
    - .source

dsc:buster:
  extends:
    - .dsc
  image: debian:buster

dsc:bullseye:
  extends:
    - .dsc
  image: debian:bullseye

# -- build ------------------

.build:rpm:
  extends:
    - .rhel:rpm
  stage: build
  variables:
    EPEL: "true"
    SRPM: "lalapps*.src.rpm"

rpm:el7:
  extends:
    - .build:rpm
  image: igwn/base:el7-testing
  needs:
    - srpm:el7

.build:deb:
  extends:
    - .debian:deb
  stage: build
  variables:
    DSC: "lalapps*.dsc"

deb:buster:
  extends:
    - .build:deb
  image: igwn/base:buster
  needs:
    - dsc:buster

# -- lint ---------------------------------------
#
# Run some quality checks on the source code
#

# job template for lint jobs
.lint:
  stage: lint
  needs: []
  only:
    variables:
      # allow these jobs to be skipped by including
      # `[skip lint]` in the commit message
      - $CI_COMMIT_MESSAGE !~ /\[skip lint\]/

lint:python:
  extends:
    - .python:flake8
    - .lint
  # use everything we get from .python:flake8

lint:coala:
  extends:
    - .lint
  image: coala/base
  script:
    # run first for codeclimate (using --json)
    - coala --ci --json -o coala.json || true
    # run again to get plaintxt output for the user
    # (and the exit code)
    - coala --ci
  after_script:
    - |
      python3 - <<EOF
      import json
      import sys
      SEVERITY = ['info', 'minor', 'major', 'critical', 'blocker']
      with open('coala.json', 'r') as file:
          indata = json.load(file)
      outdata = []
      for key in indata['results']:
          for e in indata['results'][key]:
              start = e['affected_code'][0]['start']
              end = e['affected_code'][0]['end']
              outdata.append({
                  'type': 'issue',
                  'check_name': e['origin'],
                  'content': e['additional_info'] or None,
                  'description': e['message'],
                  'fingerprint': e['id'],
                  'severity': SEVERITY[e['severity']],
                  'location': {
                      'path': start['file'],
                      'begin': start['line'],
                      'end': end['line'],
                  },
              })
      with open('codequality.json', 'w') as file:
          json.dump(outdata, file, separators=(',', ':'))
      EOF

  artifacts:
    paths:
      - coala.json
      - codequality.json
    reports:
      codequality: codequality.json
    when: always

rpmlint:
  extends:
    - .rhel:lint
  stage: lint
  needs:
    - rpm:el7

lintian:
  extends:
    - .debian:lint
  stage: lint
  needs:
    - deb:buster
